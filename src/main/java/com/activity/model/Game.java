package com.activity.model;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;

/**
 * Created by Cosmin on 07-Feb-18.
 */

@Entity
@Table(name = "GAME")
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses=Game.class)
public class Game {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "ID_BOARD")
    private String idBoard;

    @Column(name = "TEAM_NUMBER")
    private int teamNumber;

    public Game() {
    }

    public Game(String id, String idBoard, int teamNumber) {
        this.id = id;
        this.idBoard = idBoard;
        this.teamNumber = teamNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }
}
