package com.activity.model;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;

/**
 * Created by Cosmin on 07-Feb-18.
 */

@Entity
@Table(name = "CARD")
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses=Card.class)
public class Card {

    @Id
    @Column(name="ID")
    private String id;

    @Column(name="POINTS")
    private int points;


    public Card() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
