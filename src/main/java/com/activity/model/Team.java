package com.activity.model;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;
/**
 * Created by Cosmin on 07-Feb-18.
 */

@Entity
@Table(name = "TEAM")
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses=Team.class)
public class Team {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ID_GAME")
    private String idGame;

    @Column(name = "ID_CELL")
    private String idCell;


    public Team() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdGame() {
        return idGame;
    }

    public void setIdGame(String idGame) {
        this.idGame = idGame;
    }

    public String getIdCell() {
        return idCell;
    }

    public void setIdCell(String idCell) {
        this.idCell = idCell;
    }

    @Override
    public String toString() {
        return "Team Name: '" + this.name;
    }

}
