package com.activity.model;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Cosmin on 07-Feb-18.
 */

@Entity
@Table(name = "BOARD")
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses= Board.class)
public class Board {

    @Id
    @Column(name = "ID")
    private String id;


    public Board() {
    }

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

}
