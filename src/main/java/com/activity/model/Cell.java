package com.activity.model;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CELL")
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses=Cell.class)
public class Cell {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "ID_BOARD")
    private String idBoard;

    @Column(name = "POSITION")
    private int position;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "DIFFICULTY")
    private String difficulty;

    public Cell() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }
}
