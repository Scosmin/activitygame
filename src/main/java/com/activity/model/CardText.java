package com.activity.model;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;

/**
 * Created by Cosmin on 07-Feb-18.
 */

@Entity
@Table(name = "CARD_TEXT")
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses=CardText.class)
public class CardText {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "ID_CARD")
    private String idCard;

    @Column(name = "TEXT1")
    private String easySpeaking;

    @Column(name = "TEXT2")
    private String hardSpeaking;

    @Column(name = "TEXT3")
    private String easyDrawing;

    @Column(name = "TEXT4")
    private String hardDrawing;

    @Column(name = "TEXT5")
    private String easyMiming;

    @Column(name = "TEXT6")
    private String hardMiming;

    @Column(name = "IS_RED")
    private int redSquare;

    public CardText() {
    }

    public int getRedSquare() {
        return redSquare;
    }

    public void setRedSquare(int redSquare) {
        this.redSquare = redSquare;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getEasySpeaking() {
        return easySpeaking;
    }

    public void setEasySpeaking(String easySpeaking) {
        this.easySpeaking = easySpeaking;
    }

    public String getHardSpeaking() {
        return hardSpeaking;
    }

    public void setHardSpeaking(String hardSpeaking) {
        this.hardSpeaking = hardSpeaking;
    }

    public String getEasyDrawing() {
        return easyDrawing;
    }

    public void setEasyDrawing(String easyDrawing) {
        this.easyDrawing = easyDrawing;
    }

    public String getHardDrawing() {
        return hardDrawing;
    }

    public void setHardDrawing(String hardDrawing) {
        this.hardDrawing = hardDrawing;
    }

    public String getEasyMiming() {
        return easyMiming;
    }

    public void setEasyMiming(String easyMiming) {
        this.easyMiming = easyMiming;
    }

    public String getHardMiming() {
        return hardMiming;
    }

    public void setHardMiming(String hardMiming) {
        this.hardMiming = hardMiming;
    }
}
