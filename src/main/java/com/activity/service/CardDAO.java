package com.activity.service;

import com.activity.model.Card;
import com.activity.model.CardText;
import com.activity.model.Cell;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class CardDAO {


    @PersistenceContext
    EntityManager entityManager;


    public List<CardText> getAllTexts() {
        List<Object[]> queryResult = entityManager.createNativeQuery("SELECT * FROM CARD_TEXT").getResultList();
        List<CardText> textList = new ArrayList<>();
        for (Object[] iterator : queryResult) {
            CardText cardText = new CardText();
            cardText.setId(String.valueOf(iterator[0]));
            cardText.setIdCard(String.valueOf(iterator[1]));
            cardText.setRedSquare(Integer.parseInt(String.valueOf(iterator[8])));
            cardText.setEasyDrawing(String.valueOf(iterator[2]));
            cardText.setEasyMiming(String.valueOf(iterator[4]));
            cardText.setEasySpeaking(String.valueOf(iterator[3]));
            cardText.setHardDrawing(String.valueOf(iterator[5]));
            cardText.setHardSpeaking(String.valueOf(iterator[6]));
            cardText.setHardMiming(String.valueOf(iterator[7]));
            textList.add(cardText);
        }

        return textList;
    }
}