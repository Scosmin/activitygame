package com.activity.service;

import com.activity.model.Game;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by Cosmin on 05-May-18.
 */

@Repository
@Transactional
public class GameDAO {


    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Adds the game to the database
     *
     * @param game the object that we insert in the database
     * @throws Exception if something happens
     */
    public void addGame(Game game) {
        entityManager.persist(game);
    }

    public void updateGame(Game game) {
        entityManager.merge(game);
    }
}
