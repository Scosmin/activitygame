package com.activity.service;


import com.activity.model.Cell;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class CellDAO {


    @PersistenceContext
    private EntityManager entityManager;


    public List<List> getAllCells() {
        List<Object[]> queryResult = entityManager.createNativeQuery("SELECT * FROM CELL").getResultList();
        return transformQueryResult(queryResult);
    }

    public List<List> transformQueryResult(List<Object[]> queryResult) {
        List<List> list = new ArrayList<>();
        List<Cell> cellList = new ArrayList<>();
        int counter = 0;
        for (Object[] iterator : queryResult) {
            if (counter < 3) {
                Cell cell = new Cell();
                cell.setId(String.valueOf(iterator[0]));
                cell.setDifficulty(String.valueOf(iterator[3]));
                cell.setPosition(Integer.parseInt(String.valueOf(iterator[1])));
                cell.setType(String.valueOf(iterator[2]));
                cell.setIdBoard(String.valueOf(iterator[4]));
                cellList.add(cell);
                counter++;
            } else {
                Cell cell = new Cell();
                cell.setId(String.valueOf(iterator[0]));
                cell.setDifficulty(String.valueOf(iterator[3]));
                cell.setPosition(Integer.parseInt(String.valueOf(iterator[1])));
                cell.setType(String.valueOf(iterator[2]));
                cell.setIdBoard(String.valueOf(iterator[4]));
                cellList.add(cell);
                list.add(cellList);
                cellList = new ArrayList<>();
                counter = 0;
            }
        }
        return list;
    }
}
