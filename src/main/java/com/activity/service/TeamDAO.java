package com.activity.service;

import com.activity.model.Team;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by Cosmin on 05-May-18.
 */

@Repository
@Transactional
public class TeamDAO {


    @PersistenceContext
    private EntityManager entityManager;

    public void addTeam(Team team) {
        entityManager.persist(team);
    }

    public Object validateTeam(Team team) {
        return entityManager.find(Team.class, team.getId());
    }
}
