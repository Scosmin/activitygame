package com.activity.controller;

import com.activity.model.Game;
import com.activity.model.Team;
import com.activity.service.GameDAO;
import com.activity.service.TeamDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.web.context.WebApplicationContext.SCOPE_GLOBAL_SESSION;

/**
 * Created by Cosmin on 26-Mar-18.
 */

@Component
@Scope(SCOPE_GLOBAL_SESSION)
@ManagedBean
public class AddTeamBackingBean {

    private int teamNumber;
    private String gameUniqueID = UUID.randomUUID().toString();
    private String teamOneUniqueID = UUID.randomUUID().toString();
    private String teamTwoUniqueID = UUID.randomUUID().toString();
    private String teamTreeUniqueID = UUID.randomUUID().toString();
    private String teamFourUniqueID = UUID.randomUUID().toString();
    private List<Team> teamList = new ArrayList<>();
    private Team teamOne = new Team();
    private Team teamTwo = new Team();
    private Team teamTree = new Team();
    private Team teamFour = new Team();
    private Game game = new Game();
    @Autowired
    private TeamDAO teamDAO;
    @Autowired
    private GameDAO gameDAO;
    private int test = 0;

    public int getTest() {
        return test;
    }

    public String submit() {
//@TODO
// clean this mess

        game.setId(gameUniqueID);
        game.setIdBoard("1");
        game.setTeamNumber(teamNumber);
        if (teamList.size() == 0) {
            gameDAO.addGame(game);
        }
        teamOne.setIdGame(gameUniqueID);
        teamOne.setId(teamOneUniqueID);
        teamOne.setIdCell("0");
        teamTwo.setIdGame(gameUniqueID);
        teamTwo.setId(teamTwoUniqueID);
        teamTwo.setIdCell("0");
        teamTree.setIdGame(gameUniqueID);
        teamTree.setId(teamTreeUniqueID);
        teamTree.setIdCell("0");
        teamFour.setIdGame(gameUniqueID);
        teamFour.setId(teamFourUniqueID);
        teamFour.setIdCell("0");
        teamList.clear();
        switch (teamNumber) {
            case 2:
                teamList.add(teamOne);
                teamList.add(teamTwo);
                break;
            case 3:
                teamList.add(teamOne);
                teamList.add(teamTwo);
                teamList.add(teamTree);
                break;
            case 4:
                teamList.add(teamOne);
                teamList.add(teamTwo);
                teamList.add(teamTree);
                teamList.add(teamFour);
                break;
        }


        test = 1;
        return "team.jsf";
    }


    public String update() {
//@TODO
//Need to check if the teams change after I submit multiple times
        for (Team team : teamList) {
            if (teamDAO.validateTeam(team) == null) {
                teamDAO.addTeam(team);
            }
        }
        game.setTeamNumber(teamList.size());
        gameDAO.updateGame(game);
        System.out.println(teamOne + "  " + teamTwo);
        test = 0;
        return "team.jsf";
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }

    public Team getTeamOne() {
        return teamOne;
    }

    public void setTeamOne(Team teamOne) {
        this.teamOne = teamOne;
    }

    public void showTeams() {
//only for testing
        test = 2;
    }

    public Team getTeamTwo() {
        return teamTwo;
    }

    public void setTeamTwo(Team teamTwo) {
        this.teamTwo = teamTwo;
    }

    public Team getTeamTree() {
        return teamTree;
    }

    public void setTeamTree(Team teamTree) {
        this.teamTree = teamTree;
    }

    public Team getTeamFour() {
        return teamFour;
    }

    public void setTeamFour(Team teamFour) {
        this.teamFour = teamFour;
    }
}
