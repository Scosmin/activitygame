package com.activity.controller;

import com.activity.model.CardText;
import com.activity.service.CardDAO;
import com.activity.service.CellDAO;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

/**
 * Created by Cosmin on 08-Feb-18.
 */
@Component
@Scope(SCOPE_REQUEST)
@ManagedBean
public class BoardBackingBean {

    @Autowired
    private CellDAO cellDAO;

    @Autowired
    private CardDAO cardDAO;

    private static List<List> listList = new ArrayList<>();
    private static List<CardText> textList = new ArrayList<>();
    private static List<CardText> cardList = new ArrayList<>();
    private static String render = "a";
    private Random randomGenerator = new Random();
    private static String diceOne;
    private static String diceTwo;

    @PostConstruct
    public void init() {
        listList = cellDAO.getAllCells();
        textList = cardDAO.getAllTexts();

    }


    public List<List> getListList() {

        return listList;
    }

    public List<CardText> getTextList() {
        return textList;
    }

    public void setRender(String a) {
        render = a;
    }

    public String getRender() {
        return render;
    }

    public void initCard() {
        List<CardText> test = new ArrayList<>();
        test=cardList;
        cardList = new ArrayList<>();
        int a = test.size();
        int index = randomGenerator.nextInt(a);
        cardList.add(test.get(index));
    }

    public void initializeSpecificPointsCard(String points) {
        cardList.clear();
        for (CardText iterator : textList) {
            if (iterator.getIdCard().equals(points)) {
                cardList.add(iterator);
            }
        }
    }

    public List<CardText> getCardList() {
        return cardList;
    }

    public String getFourPointsCard() {
        initializeSpecificPointsCard("2");
        initCard();
        return "board.jsf";
    }

    public String getThreePointsCard() {
        initializeSpecificPointsCard("1");
        initCard();
        return "board.jsf";
    }

    public String getFivePointsCard() {
        initializeSpecificPointsCard("3");
        initCard();
        return "board.jsf";
    }

    public void setDices() {
        int randomNumOne = ThreadLocalRandom.current().nextInt(1, 7);
        int randomNumTwo = ThreadLocalRandom.current().nextInt(1, 7);
        diceOne=String.valueOf(randomNumOne);
        diceTwo=String.valueOf(randomNumTwo);
    }

    public String movePawn(){
        return "board.jsf";
    }
    public String getDiceOne(){
        return diceOne;
    }

    public String getDiceTwo(){
        return diceTwo;
    }
}
